//--------------------------------------------------------------------------------------
// File:    TPSampleDataAcqComponent.cpp
// Desc:    This file implements a basic data acquisition I/O driver plug-in component.
//          This component does nothing interesting, but can be used as a base-line for
//          your own implementation.
//
//          Documentation should have been distributed with the TunerPro Developer SDK.
//          Also make sure to study ITPPlugin.h for more information on the plug-in
//          architecture.
//
// Copyright (c) Mark Mansur. All rights reserved. This source code is *not*
// redistributable, and may be obtained only by permission from its author, Mark Mansur.
// For information, please see www.tunerpro.net, or contact mark@tunerpro.net.
//--------------------------------------------------------------------------------------
#include "stdafx.h"             // Pre-compiled header
#include "TPPluginSample.h"     // Contains the declarations for everything we implement in this file



//---------------------------------------------------------------------------------------
//                  Preferred Return Values 
//
// All returned HRESULTs should signify failure in a way that would trigger FAILED(hr);
// Similarly, success should trigger SUCCEEDED(hr);. These are what TunerPro will use
// to determine success/failure state when interacting with your component/plug-in.
// 
// S_OK - general success
// E_FAIL - generic failure (try to be more specific, if possible, though)
// E_INVALIDARG - invalid argument was passed in. Most likely TunerPro will simply fail.
// HRESULT_FROM_WIN32(ERROR_NOT_CONNECTED) - The device isn't attached or found
//
//---------------------------------------------------------------------------------------



TPDataAcqIODriver::TPDataAcqIODriver()
{
    m_bErrorPrompt      = FALSE;
    m_strLastError[0]   = '\0';
}

TPDataAcqIODriver::~TPDataAcqIODriver()
{
}

//---------------------------------------------------------------------------------------
// Func: GetComponentInfo
// Desc: Virtual implementation for plug-in component. Returns information about this
//       component.
//---------------------------------------------------------------------------------------
BOOL TPDataAcqIODriver::GetComponentInfo(TPP_COMPONENTINFO* pInfoStruct)
{
    OutputDebugString("PLUGIN SAMPLE: TPDataAcqIODriver::GetComponentInfo called");

    // TODO: Return information about your component to TunerPro
    if ( pInfoStruct )
    {
        // Inspect the size of the struct to make sure the TunerPro build that's 
        // using us is not expecting something of a different size
        if ( pInfoStruct->cbSize != sizeof(TPP_COMPONENTINFO) )
        {
            // Return an error!
        }

        UuidFromString((RPC_CSTR)DATAACQIO_COMPONENT_GUID, &pInfoStruct->ID);
        StringCbCopy(pInfoStruct->strName, TPP_MAX_NAME,
            TEXT("TunerPro Sample Data Acquisition I/O Interface"));
        StringCbCopy(pInfoStruct->strDesc, TPP_MAX_DESC,
            TEXT("This component shows you how to write a basic data acquisition driver for TunerPro RT."));
        StringCbCopy(pInfoStruct->strVersion, TPP_MAX_VERSION,
            TEXT("1.0.0"));

        // Tell TunerPro to allow the user to configure this plug-in.
        pInfoStruct->bConfigurable = TRUE;

        // Provide TunerPro with an easy-to-compare set of version numbers. Strings aren't easy
        // to compare, but are easy for users to read.
        pInfoStruct->wVersionMajor = 1;
        pInfoStruct->wVersionMinor = 0;
        return TRUE;
    }
    return FALSE;
}

//---------------------------------------------------------------------------------------
// Func: Configure
// Desc: Virtual implementation for plug-in component. Allows the user to configure the
//       component.
//---------------------------------------------------------------------------------------
HRESULT TPDataAcqIODriver::Configure(TPP_CONFIGINFO* pConfigInfo)
{
    OutputDebugString("PLUGIN SAMPLE: TPDataAcqIODriver::Configure called");

    // TODO: Allow the user to configure this component, if possible/necessary.
    if ( pConfigInfo )
    {
        // Inspect the size of the struct to make sure the TunerPro build that's 
        // using us is not expecting something of a different size
        if ( pConfigInfo->cbSize != sizeof(TPP_CONFIGINFO) )
        {
            // Return an error!
        }

        MessageBox(pConfigInfo->hwndOwner, "User wants to configure the data acq. driver!",
            "Component Configuration Requested", MB_ICONINFORMATION);
    }
    return S_OK;
}

//---------------------------------------------------------------------------------------
// Func: MessageHandler
// Desc: TunerPro can send us messages to notify us of an event, or to request info
//       from us. This also allows TunerPro to extend functionality without breaking the
//       plug-in interface.
//---------------------------------------------------------------------------------------
LONG_PTR TPDataAcqIODriver::MessageHandler(UINT uiMsg, LPARAM lParam1, LPARAM lParam2)
{
    // TODO: handle messages
    return 0;
}

//---------------------------------------------------------------------------------------
// Func: GetLastErrorText
// Desc: Virtual implementation for plug-in component. App calls this upon error to
//       determine whether friendly error should be posted to user, and what text to post.
//---------------------------------------------------------------------------------------
const CHAR* TPDataAcqIODriver::GetLastErrorText(BOOL& bPrompt)
{
    bPrompt = m_bErrorPrompt;

    // TODO: Return the last error. If the error should be bubbled up to the user,
    // return TRUE in bPrompt.
    OutputDebugString("PLUGIN SAMPLE: TPDataAcqIODriver::GetLastErrorText called");

    return m_strLastError;
}


//---------------------------------------------------------------------------------------
// Func: GetHardwareInfo
// Desc: This is called by TunerPro to display to the user information on the attached
//       hardware.
//---------------------------------------------------------------------------------------
HRESULT TPDataAcqIODriver::GetHardwareInfo(TPDATAACQIOCAPS* pCaps)
{
    OutputDebugString("PLUGIN SAMPLE: TPDataAcqIODriver::GetHardwareInfo called");

    // TODO: return real information about your hardware. Here's a fake example
    if ( pCaps )
    {
        // Inspect the size of the struct to make sure the TunerPro build that's 
        // using us is not expecting something of a different size
        if ( pCaps->cbSize != sizeof(TPDATAACQIOCAPS) )
        {
            // Return an error!
        }

        StringCbCopy(pCaps->strName, TPP_MAX_NAME, "Fake Data Acq Hardware");

        // No description.
        pCaps->strDescription[0] = 0;
        return S_OK;
    }
    else
        return HRESULT_FROM_WIN32(ERROR_INVALID_PARAMETER);
}

//---------------------------------------------------------------------------------------
// Func: InitializeHardware
// Desc: TunerPro will call this function when the acquisition hardware should be
//       initialized and set up to be ready to do work.
//---------------------------------------------------------------------------------------
HRESULT TPDataAcqIODriver::InitializeHardware(TPDATAACQHWINIT* pInitStruct)
{
    HRESULT hr = S_OK;

    OutputDebugString("PLUGIN SAMPLE: TPDataAcqIODriver::GetHardwareInfo called");

    if ( pInitStruct )
    {
        // Inspect the size of the struct to make sure the TunerPro build that's 
        // using us is not expecting something of a different size
        if ( pInitStruct->cbSize != sizeof(TPDATAACQHWINIT) )
        {
            // Return an error!
        }

        // TODO: Initialize the data acquisition hardware if necessary
        OutputDebugString("PLUGIN SAMPLE: TPDataAcqIODriver::InitializeHardware called");
    }

    return hr;
}

//---------------------------------------------------------------------------------------
// Func: ReleaseHardware
// Desc: TunerPro will call this when the hardware is no longer needed and 
//       should be released.
//---------------------------------------------------------------------------------------
HRESULT TPDataAcqIODriver::ReleaseHardware()
{
    HRESULT hr = S_OK;
    
    // TODO: release hardware, memory, etc, as necessary.
    OutputDebugString("PLUGIN SAMPLE: TPDataAcqIODriver::ReleaseHardware called");

    return hr;
}

//---------------------------------------------------------------------------------------
// Func: WriteData
// Desc: TunerPro's data acquisition engine will call this method and pass to it the
//       data to write to the device. You could potentially intercept this data and modify
//       it before sending, or even use the data as a command that you process that 
//       triggers other behavior that you implement. In this way you can abstract the
//       complex inner workings of your interface to the definition writer.
//---------------------------------------------------------------------------------------
HRESULT TPDataAcqIODriver::WriteData(TPDATAACQWRITE* pWriteInfo)
{
    HRESULT hr = S_OK;
    DWORD dwIO = 0;

    if ( !pWriteInfo )
        return E_INVALIDARG;

    // TODO: Write the data to the port. OR you can intercept the data here
    // and write something else to the port. For instance, if you wanted to have 
    // custom commands set up in the ADX definition, here is where you would 
    // intercept those commands and do work based on the command.
    //
    // This is where some of the real power of the data acq driver comes into play.

    OutputDebugString("PLUGIN SAMPLE: TPDataAcqIODriver::WriteData called");

    // Optionally, tell TunerPro how much data was written.
    if ( pWriteInfo->puiTransferred )
        *pWriteInfo->puiTransferred = dwIO;

    return hr;
}

//---------------------------------------------------------------------------------------
// Func: ReadData
// Desc: TunerPro's data acquisition engine will call this method and pass to it the
//       amount of data to retrieve and the buffer to copy it to.
//---------------------------------------------------------------------------------------
HRESULT TPDataAcqIODriver::ReadData(TPDATAACQREAD* pReadInfo)
{
    HRESULT hr = S_OK;
    DWORD dwIO = 0;

    if ( !pReadInfo )
        return E_INVALIDARG;

    // TODO: Collect data from the device and send it back to Tunerpro.

    OutputDebugString("PLUGIN SAMPLE: TPDataAcqIODriver::ReadData called");

    // Optionally, tell TunerPero how much data was read
    if ( pReadInfo->puiTransferred )
        *pReadInfo->puiTransferred = dwIO;
    return hr;
}

//---------------------------------------------------------------------------------------
// Func: SetTimeouts
// Desc: Called when TunerPro's data acquisition engine needs to change the timeouts
//       for tx/rx. If your plug-in wraps a serial port, this call should result in a 
//       call to SetCommTimeouts. If not, then you should implement and enforce these
//       timeouts yourself. Timeouts can be very critical to a successful connection for
//       some vehicles, etc.
//---------------------------------------------------------------------------------------
HRESULT TPDataAcqIODriver::SetTimeouts(TPDATAACQTIMEOUTS* pTimeoutInfo)
{
    HRESULT hr = S_OK;

    // TODO: Store the timeouts to be enforced by your driver, or the underlying
    // device driver (e.g. serial driver, USB driver, etc) for the device. If the
    // underlying device driver doesn't have such a concept, it is recommended that
    // you implement these timeouts yourself.

    OutputDebugString("PLUGIN SAMPLE: TPDataAcqIODriver::SetTimeouts called");

    return hr;
}

//---------------------------------------------------------------------------------------
// Func: GetTimeouts
// Desc: Called when TunerPro's data acquisition engine needs to retrieve the timeouts
//       for tx/rx. If your plug-in wraps a serial port, this call should result in a 
//       call to GetCommTimeouts. If not, then you should implement and enforce these
//       timeouts yourself. Timeouts can be very critical to a successful connection for
//       some vehicles, etc.
//---------------------------------------------------------------------------------------
HRESULT TPDataAcqIODriver::GetTimeouts(TPDATAACQTIMEOUTS* pTimeoutInfo)
{
    HRESULT hr = S_OK;

    // TODO: Return the timeouts to be enforced by your driver, or the underlying
    // device driver (e.g. serial driver, USB driver, etc) for the device. If the
    // underlying device driver doesn't have such a concept, it is recommended that
    // you implement these timeouts yourself.

    OutputDebugString("PLUGIN SAMPLE: TPDataAcqIODriver::GetTimeouts called");

    return hr;
}

//---------------------------------------------------------------------------------------
// Func: PurgeReceiveBuffer
// Desc: Called when TunerPro's data acquisition engine needs to purge the receive
//       FIFO buffer. 
//---------------------------------------------------------------------------------------
HRESULT TPDataAcqIODriver::PurgeReceiveBuffer()
{
    HRESULT hr = S_OK;

    // TODO: Purge the receive first-in first-out buffer if there is one (and using one
    // is recommended).
    
    OutputDebugString("PLUGIN SAMPLE: TPDataAcqIODriver::PurgeReceiveBuffer called");

    return hr;
}

//---------------------------------------------------------------------------------------
// Func: PurgeTransmitBuffer
// Desc: Called when TunerPro's data acquisition engine needs to purge the transmit
//       FIFO buffer.
//---------------------------------------------------------------------------------------
HRESULT TPDataAcqIODriver::PurgeTransmitBuffer()
{
    HRESULT hr = S_OK;

    // TODO: Purge the transmit first-in first-out buffer if there one (and using one is
    // recommended).

    OutputDebugString("PLUGIN SAMPLE: TPDataAcqIODriver::PurgeTransmitBuffer called");

    return hr;
}