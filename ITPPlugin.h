//--------------------------------------------------------------------------------------
// File:    ITPPlugin.h
// Desc:    Defines all TunerPro RT plug-in interfaces.
//
// Hist:    5/31/10 - Contract v2. Added stop bits, parity, and byte size
//              support to data acquisition hardware initialization
//          7/12/10 - Contract v3. Added RS232 echo to data acq hw init, and
//              added more EMU cap flags (read-only)
//
// Copyright (c) Mark Mansur. All rights reserved. This source code is *not*
// redistributable, and may be obtained only by permission from its author, Mark Mansur.
// For information, please see www.tunerpro.net, or contact mark@tunerpro.net.
//--------------------------------------------------------------------------------------
#pragma once
#ifndef _ITPPLUGIN_H
#define _ITPPLUGIN_H

// Header defining TunerPro's application interfaces. TunerPro will register its main
// interface with the plug-in by sending TPM_REGISTER_TUNERPRO_INTERFACE to the plug-in
// object's MessageHandler function. That interface is the plug-in's gateway to any
// functionality that TunerPro exposes to plug-ins.
#include "ITunerPro.h"

// Header defining all messages and notifications that can be sent by TunerPro to
// plug-in and component objects, and from those objects to TunerPro.
#include "TPMessages.h"

// Current plug-in contract version, used to ensure that newer contracts
// aren't used with older TunerPro versions. TPP_PLUGININFO::dwContractVersion
// should always be set to this macro.
#define TPPLUGIN_CONTRACT_VERSION   3


//---------------------------------------------------------------------------------------
// Extern "C" functions that the DLL must implement in order to be recognized by
// TunerPro. These are provided for example purposes, and are only defined in your plug-in
// DLL, hence the commented declarations.
//---------------------------------------------------------------------------------------

// Plug-in factory function
//extern "C" ITPPlugin* TPCreatePlugin();

// Plug-in cleanup function
//extern "C" void TPReleasePlugin(ITPPlugin* pPlugin);


// Forward declaration of items needed by the class below, but defined later 
// in this file. For architectural clarity, the ordering defined in this file
// starts with base class(es) and continues down the inheritance chain.
class ITPPluginComponent;
enum TPPCOMPONENT_TYPE;

// Structure returned by a plug-in for identification, information, capabilities, etc.
// This struct may grow in the future, but members should only be added to the end for
// backward compatibility
#define TPP_MAX_NAME        50
#define TPP_MAX_DESC        512
#define TPP_MAX_VERSION     15
#define TPP_MAX_AUTHOR      100

struct TPP_PLUGININFO
{
    DWORD   cbSize;                         // Set by caller to sizeof(TPP_PLUGININFO). May be used by plug-in for host app compatibility check
    DWORD   dwContractVersion;              // Plug-in should initialize this to TPPLUGIN_CONTRACT_VERSION
    GUID    ID;                             // GUID that identifies the plug-in. Plug-in dev should create this and hard code it.
    CHAR    strName[TPP_MAX_NAME];          // Plug-in defined name
    CHAR    strDesc[TPP_MAX_DESC];          // Plug-in defined description
    CHAR    strVersion[TPP_MAX_VERSION];    // Plug-in defined version string
    CHAR    strAuthor[TPP_MAX_AUTHOR];      // Plug-in author
    DWORD   dwComponentCount;               // Plug-in should set this to the number of components it contains
    BOOL    bConfigurable;                  // Plug-in can be configured. Configure method used for this.
    WORD    wVersionMajor;                  // Internal plug-in major version number
    WORD    wVersionMinor;                  // Internal plug-in minor version number
};

//---------------------------------------------------------------------------------------
// Plug-in & Component Configuration struct used for the Configure methods. Configuration
// allows the plug-in to provide a user interface that the user can use to set up the
// plug-in or component. It's up to the plug-in developer to store and retrieve the
// configuration as necessary, and to start with reasonable defaults.
//---------------------------------------------------------------------------------------
struct TPP_CONFIGINFO
{
    DWORD   cbSize;         // Set by caller to sizeof(TPP_CONFIGINFO). May be used by plug-in for host app compatibility check
    HWND    hwndOwner;      // The parent window of the configuration window
};

//---------------------------------------------------------------------------------------
//  The base plug-in interface
//
//  In a plug-in DLL, there exists only one plug-in. The plug-in can contain/manage multiple
//  components. For instance, a single plug-in could have an emulation hardware "driver"
//  component and a data acquisition interface "driver" component.
//
//  All plug-ins must at least implement this interface
//---------------------------------------------------------------------------------------
class ITPPlugin
{
public:

    virtual ~ITPPlugin() {}

    // Allows TunerPro to retrieve info about the plug-in
    virtual BOOL GetPluginInfo(IN OUT TPP_PLUGININFO* pInfoStruct)              = 0;

    // Allows user to configure the plug-in
    virtual HRESULT Configure(IN TPP_CONFIGINFO* pConfigInfo)                   = 0;

    // Get and release components of the plug-in
    virtual ITPPluginComponent* GetComponent(IN UINT uiIndex)                   = 0;
    virtual VOID ReleaseComponent(IN ITPPluginComponent* pComponent)            = 0;

    // Allows TunerPro to send component messages and notifications
    virtual LONG_PTR MessageHandler(UINT uiMsg, LPARAM lParam1, LPARAM lParam2) = 0;
};



//---------------------------------------------------------------------------------------
//  Plug-in Component
// 
//  A plug-in component is the part of the plug-in that does work. A plug-in can contain
//  multiple components. TunerPro will open your plug-in and query it for component
//  interfaces. If a component does not exist, the plug-in should return NULL when queried
//  for the component. Otherwise it should instantiate the component and return the 
//  pointer to it. It is up to the plug-in developer to choose (and implement) singleton
//  vs. multi-instance components.
//---------------------------------------------------------------------------------------
enum TPPCOMPONENT_TYPE
{
    TPPLUGINCOMPONENT_NONE,         // None specified. This is an error.

    // 1 - 99999 are reserved

    TPPLUGINCOMPONENT_EMULATION_DRIVER		= 100000,
    TPPLUGINCOMPONENT_DATAACQIO_DRIVER,
};

struct TPP_COMPONENTINFO
{
    DWORD   cbSize;                         // Size of this struct. Set by caller, inspected by component.
    GUID    ID;                             // GUID that identifies the component. Component dev to create and hard-code
    CHAR    strName[TPP_MAX_NAME];          // Plug-in defined name
    CHAR    strDesc[TPP_MAX_DESC];          // Plug-in defined description
    CHAR    strVersion[TPP_MAX_VERSION];    // Plug-in defined version string
    BOOL    bConfigurable;                  // Component can be user-configured. Configure method is used for this.
    WORD    wVersionMajor;                  // Internal plug-in major version number
    WORD    wVersionMinor;                  // Internal plug-in minor version number
};

class ITPPluginComponent
{
public:

    virtual ~ITPPluginComponent() {}

    // Returns the component type so TunerPro can identify it later if necessary
    virtual TPPCOMPONENT_TYPE GetType()                                         = 0;

    // Allows application to retrieve info about the component
    virtual BOOL GetComponentInfo(IN OUT TPP_COMPONENTINFO* pInfoStruct)        = 0;

    // Allows user to configure the component
    virtual HRESULT Configure(IN TPP_CONFIGINFO* pConfigInfo)                   = 0;

    // Allows TunerPro to send component messages and notifications
    virtual LONG_PTR MessageHandler(UINT uiMsg, LPARAM lParam1, LPARAM lParam2) = 0;

    // Returns a descriptive error message for last error, and tells TunerPro whether to prompt
    virtual const CHAR* GetLastErrorText(OUT BOOL& bPrompt)                     = 0;
};



//
//---------------------------------------------------------------------------------------
//  The various component interfaces. 
//---------------------------------------------------------------------------------------
//



//---------------------------------------------------------------------------------------
//  Emulation driver component definition
//---------------------------------------------------------------------------------------

// Hardware capability flags. These are generic capabilities that TunerPro can make use of.
#define TPP_EMU_CAP_CHIPEMULATION               0x00000001  // Device holds chip data and sits in place of chip
#define TPP_EMU_CAP_REALTIMEMULATION			0x00000002  // Data can be uploaded incrementally while the vehicle is running
#define TPP_EMU_CAP_SWBANKSWITCHING				0x00000004  // Banks can be selected within the software UI
#define TPP_EMU_CAP_ADDRESSWATCH				0x00000008  // Device supports address hit tracing
#define TPP_EMU_CAP_WRITEONLY                   0x00000010  // The data on the emulator cannot be read back. Disable read UI.
#define TPP_EMU_CAP_READONLY                    0x00000020  // The data on the emulator cannot be replaced. Disable write UI.
#define TPP_EMU_CAP_NOVERIFY                    0x00000040  // Data verification is impossible. Disable verification UI.

struct TPEMUCAPS
{
	DWORD	cbSize;					        // Initialize to sizeof(TPEMUCAPS)
    CHAR    strName[TPP_MAX_NAME];          // The name of the hardware
    CHAR    strDescription[TPP_MAX_DESC];   // Hardware description
    CHAR    strVersion[TPP_MAX_VERSION];    // Hardware version string
    DWORD   dwCapFlags;                     // Hardware capabilities (see flags above)
    UINT    uiTotalMemorySize;              // The total memory size of the hardware
    UINT    uiBankCount;                    // The number of banks the hardware supports
};

//
// Trace address hit callback (defined by TunecrPro and passed into the component). The component should call this
// when trace data is received by it.
//
struct TPTRACEDATA
{
    DWORD  cbSize;           // set by the caller to sizeof(TPTRACEDATA);
    DWORD* pdwAddresses;
    DWORD  dwAddressCount;
    DWORD  dwFlags;
};
typedef VOID (*PFNTRACECALLBACK)(TPTRACEDATA* pTraceData);  


//
// Trace entry (data to be passed to BeginTrace)
//

// Trace mode flags
#define TPP_EMU_TRACE_NONREDUNDANT          0x00000001  // If set, sequential repeated hits are ignored. Only first is returned
#define TPP_EMU_TRACE_SYNCHRONOUS           0x00000002  // If set, BeginTrace only exits after data is gathered
#define TPP_EMU_TRACE_ADDR_START_RELATIVE   0x00000004  // If set, returned address are offsets from the start 

// The trace initialization struct
struct TPTRACEINIT
{
    DWORD cbSize;                   // Initialize to the size of TRACEINIT
    DWORD* pdwAddresses;            // Packet buffer to receive hits when called synchronously (not used for asynch)
    UINT  cAddresses;               // Number of addresses per packet to accumulate. pdwAddresses should be at least this size (in DWORDs)
    DWORD dwStartAddress;           // Start location of the window to monitor
    DWORD dwEndAddress;             // End location of the window to monitor
    DWORD dwPacketCount;            // The number of packets to gather before exiting trace mode automatically (0 == manual stop)
    PFNTRACECALLBACK pfnCallback;   // The app callback to call when hits are acquired
    DWORD dwModeFlags;              // Flags specifying mode
};

//
// Emulation read/write parameters 
//

// Read flags
#define TPP_EMU_READ_RAWDATA        0x00000001  // If specified, a raw read is to be issued directly to the device port

struct TPEMUREAD
{
    DWORD cbSize;                       // Initialized by the caller to sizeof(TPEMUREAD)
    BYTE* pBuffer;                      // Pointer to the buffer to receive the data
    UINT  cbReadSize;                   // Amount of data to read
    DWORD dwAddress;                    // Address from which to read
    UINT* puiTransferred;               // Amount of data read
    DWORD dwFlags;                      // Read flags.
    ITPProgress* pIProgress;            // Pointer to progress interface
};

// Write flags
#define TPP_EMU_WRITE_RAWDATA       0x00000001  // If specified, the specified data is to be written directly to the device port

struct TPEMUWRITE
{
    DWORD cbSize;                       // Initialized by the caller to sizeof(TPEMUREAD)
    BYTE* pData;                        // Pointer to the buffer to write
    UINT  cbData;                       // Amount of data to write
    DWORD dwAddress;                    // Address to which to write
    UINT* puiTransferred;               // Amount of data written
    DWORD dwFlags;                      // Write flags
    ITPProgress* pIProgress;            // Pointer to progress interface
};

struct TPEMUVERIFY
{
    DWORD cbSize;                       // Initialized by the caller to sizeof(TPEMUREAD)
    BYTE* pDataToCompare;               // Pointer to the buffer containing the local data to compare
    UINT  cbData;                       // Amount of data to compare
    DWORD dwAddress;                    // Address where remote comparison data starts
    DWORD dwFlags;                      // Verify flags. Currently unused.
    ITPProgress* pIProgress;            // Pointer to progress interface
};

//---------------------------------------------------------------------------------------
//  The emulator interface
//---------------------------------------------------------------------------------------
class ITPEmulator : public ITPPluginComponent
{
public:
    virtual ~ITPEmulator() {}

    virtual HRESULT GetHardwareInfo(OUT TPEMUCAPS* pCaps)           = 0;

    virtual HRESULT InitializeHardware()                            = 0;
    virtual HRESULT ReleaseHardware()                               = 0;

    virtual HRESULT CancelOperation()                               = 0;

    virtual HRESULT GetBank(IN OUT UINT* puiBank)                   = 0;
    virtual HRESULT SetBank(IN UINT uiBank)                         = 0;
    virtual HRESULT GetCurrentBankSize(IN OUT UINT* puiSize)        = 0;

    virtual HRESULT GetJustificationOffset(IN UINT cbData, IN OUT UINT* puiOffset) = 0;

    virtual HRESULT WriteData(IN TPEMUWRITE* pWriteInfo)            = 0;
    virtual HRESULT ReadData(IN TPEMUREAD* pReadInfo)               = 0;
    virtual HRESULT VerifyData(IN TPEMUVERIFY* pVerifyInfo)         = 0;

    virtual HRESULT BeginTrace(IN TPTRACEINIT* pInitStruct)         = 0;
    virtual BOOL    IsTracing()                                     = 0;
    virtual HRESULT EndTrace()                                      = 0;
};



//---------------------------------------------------------------------------------------
// Data Acquisition I/O driver component definitions
//---------------------------------------------------------------------------------------

//
// I/O hardware information and capabilities. 
//
struct TPDATAACQIOCAPS
{
    DWORD   cbSize;
    CHAR    strName[TPP_MAX_NAME];          // The name of the hardware
    CHAR    strDescription[TPP_MAX_DESC];   // Hardware description
    CHAR    strVersion[TPP_MAX_VERSION];    // Hardware version string
    DWORD   dwCapFlags;                     // Hardware capabilities
};

//
// Initialization info, passed by TunerPro to ITPDataAcqIO::InitializeHardware.
// InitializeHardware may be called repeatedly due to baud or data config for
// different ADX commands. If the plug-in wraps a serial port and the port is
// already open from an earlier call to InitializeHardware, it may not make
// sense to close the serial port and re-open it. Instead, the plug-in may
// simply set the port up to use the new baud or data configurations.
//
struct TPDATAACQHWINIT
{
    DWORD cbSize;                   // Initialized by the caller to sizeof(TPDATAACQHWINIT);
    DWORD dwBaud;                   // bits per second to initialize the port to (may optionally be used by plug-in)
    BYTE  btParity;                 // data parity (may optionally be used by plug-in). Can be NOPARITY (0), ODDPARITY (1), EVENPARITY (2), MARKPARITY (3), or SPACEPARITY (4).
    BYTE  btBitsPerByte;            // Number of bits in the byte. 8 is standard. 7 is sometimes seen, others are very rare.
    BYTE  btStopBits;               // Can be ONESTOPBIT (0), ONE5STOPBITS (1), or TWOSTOPBITS (2)
    BOOL  bRS232Echo;               // If TRUE, all output will be echoed to input
};

//
// Transfer timeout info
//
struct TPDATAACQTIMEOUTS
{
    DWORD cbSize;                   // Initialized by the caller to sizeof(TPDATAACQTIMEOUTS);
    DWORD dwReadInterval;           // The max interval between received bytes
    DWORD dwReadTotalMult;          // The read total timeout per-byte multiplier (#bytes * mult + const)
    DWORD dwReadTotalConst;         // The read total timeout constant (#bytes * mult + const)
    DWORD dwWriteTotalMult;         // The write total timeout multiplier (#bytes * mult + const)
    DWORD dwWriteTotalConst;        // The write total timeout constant (#bytes * mult + const)
    DWORD dwFlags;                  // Flags. Currently unused
};

//
// Read request parameters
//
struct TPDATAACQREAD
{
    DWORD cbSize;                   // Initialized by the caller to sizeof(TPDATAACQREAD);
    BYTE* pBuffer;                  // Buffer to which to transfer data
    DWORD cbReadSize;               // Number of bytes to transfer
    UINT* puiTransferred;           // Number of bytes transferred
    DWORD dwFlags;                  // Read flags. Currently unused
};

//
// Write request parameters
//
struct TPDATAACQWRITE
{
    DWORD cbSize;                   // Initialized by the caller to sizeof(TPDATAACQWRITE);
    BYTE* pData;                    // Buffer containing data to transfer
    DWORD cbData;                   // Number of bytes to transfer
    UINT* puiTransferred;           // Number of bytes transferred
    DWORD dwFlags;                  // Write flags. Currently unused
};

//---------------------------------------------------------------------------------------
// Data Acquisition driver component
//---------------------------------------------------------------------------------------
class ITPDataAcqIO : public ITPPluginComponent
{
public:

    virtual ~ITPDataAcqIO() {}

    virtual HRESULT GetHardwareInfo(OUT TPDATAACQIOCAPS* pCaps)             = 0;

    virtual HRESULT InitializeHardware(IN TPDATAACQHWINIT* pInitStruct)     = 0;
    virtual HRESULT ReleaseHardware()                                       = 0;

    virtual HRESULT WriteData(IN TPDATAACQWRITE* pWriteInfo)                = 0;
    virtual HRESULT ReadData(IN TPDATAACQREAD* pReadInfo)                   = 0;

    virtual HRESULT SetTimeouts(IN TPDATAACQTIMEOUTS* pTimeoutInfo)         = 0;
    virtual HRESULT GetTimeouts(IN OUT TPDATAACQTIMEOUTS* pTimeoutInfo)     = 0;
    virtual HRESULT PurgeTransmitBuffer()                                   = 0;
    virtual HRESULT PurgeReceiveBuffer()                                    = 0;
};


#endif // _ITPPLUGIN_H