//--------------------------------------------------------------------------------------
// File:    LC1.h
// Desc:    Basic LC1 single wideband data acquisition plugin
// Copyright (c) Eric Honsch. All rights reserved.  Free for any one to use as they see fit.
//--------------------------------------------------------------------------------------
#pragma once

// TunerPro's plug-in header, which defines everything we need to implement a plug-in
#include "ITPPlugin.h"

#include "Comm.h"

//---------------------------------------------------------------------------------------
// Plug-in and Component GUIDs. These are used to identify the plug-in and component to
// TunerPro, so that multiple versions are not loaded simultaneously.
//
// EVERY plug-in and component that you create yourself should use its own set of GUIDs. 
// Do not re-use GUIDs. This is important. In Visual Studio, use the "tools->Create GUID"
// tool to generate a new GUID for every piece of your plug-in.
//---------------------------------------------------------------------------------------
#define PLUGIN_GUID     "8CC57AF1-1778-4184-8E73-81BBE9251760"
#define LC1_GUID        "7DC5537C-3D96-4be9-8B77-A88F4355A147"


//---------------------------------------------------------------------------------------
// Globals struct. These are globals to be shared across all CPP files in this project.
//  It's handy to have them live in their own struct as a collection.
//---------------------------------------------------------------------------------------
struct GLOBALS
{
    HMODULE hModule;
};

extern GLOBALS globals;


//---------------------------------------------------------------------------------------
// Enumeration of indexes of components contained in this plug-in DLL. This is used by
// GetComponent override of ITPPlugin, and is how the plug-in determines which component
// to instantiate and return to TunerPro.
//---------------------------------------------------------------------------------------
enum 
{
    LC1_DRIVER,
    TP_COMPONENT_COUNT
};



//---------------------------------------------------------------------------------------
class LC1Driver : public ITPDataAcqIO
{
public:

    LC1Driver();
   ~LC1Driver();

    //---------------------------------------------------------------------------------------
    // ITPPluginComponent Methods

    TPPCOMPONENT_TYPE GetType(void);
    BOOL        GetComponentInfo(IN OUT TPP_COMPONENTINFO *components);
    HRESULT     Configure(TPP_CONFIGINFO *config);
    LONG_PTR    MessageHandler(UINT msg, LPARAM param1, LPARAM param2);
    const CHAR *GetLastErrorText(BOOL &prompt);

    //---------------------------------------------------------------------------------------
    // ITPDataAcqIO Methods

    HRESULT GetHardwareInfo(TPDATAACQIOCAPS *caps);

    HRESULT InitializeHardware(TPDATAACQHWINIT *init);
    HRESULT ReleaseHardware(void);

    HRESULT WriteData(TPDATAACQWRITE *writeInfo);
    HRESULT ReadData(TPDATAACQREAD *readInfo);

    HRESULT SetTimeouts(TPDATAACQTIMEOUTS *timeout);
    HRESULT GetTimeouts(TPDATAACQTIMEOUTS *timeout);

    HRESULT PurgeReceiveBuffer(void);
    HRESULT PurgeTransmitBuffer(void);

    //---------------------------------------------------------------------------------------
    // accessors for configuration
    unsigned GetComPort(void) { return mComPort; }
    void     SetComPort(unsigned port);

    //---------------------------------------------------------------------------------------

private:

    CommChannel       mCommChannel;
    unsigned          mComPort;
    float             mLastO2;

    #define           INNOVATE_MAX_FRAME_SIZE (257 * 2)
    unsigned char     mInnovateFrame[INNOVATE_MAX_FRAME_SIZE];
    unsigned          mFrameWritePtr;

    bool              mConnected;

    TPDATAACQTIMEOUTS mTimeout;

    CHAR        mConfigFileName[MAX_PATH];

    CHAR        mLastError[300];    // If an error occurs while doing work in our component, we store it here
    BOOL        mErrorPrompt;       // Should the last error be bubbled up to the user?

    void LoadConfig(void);
    void SaveConfig(void);
};


//---------------------------------------------------------------------------------------
//  The base plug-in class. There should only one instance of this object per plug-in
//  DLL.
//---------------------------------------------------------------------------------------
class LC1Plugin : public ITPPlugin    
{
public:
    LC1Plugin();
    ~LC1Plugin();

    //---------------------------------------------------------------------------------------
    // ITPPlugin Methods

    BOOL                GetPluginInfo(TPP_PLUGININFO *plugin);
    HRESULT             Configure(TPP_CONFIGINFO *config);
    ITPPluginComponent *GetComponent(UINT index);
    VOID                ReleaseComponent(ITPPluginComponent *component);
    LONG_PTR            MessageHandler(UINT msg, LPARAM param1, LPARAM param2);

    //---------------------------------------------------------------------------------------
    // TODO: Your own methods (if necessary)

private: // variables, etc

    ITunerProApp *mITunerPro;       // TunerPro's app interface
    LC1Driver     mLC1Driver;
};



