//-----------------------------------------------------------------------------
//  Comm.cpp
//  Brain-dead serial port wrapper for Win32
//  Copyright Eric Honsch.  Free for any one to use as they see fit
//-----------------------------------------------------------------------------

#include "Comm.h"

#include <stdio.h>

//-----------------------------------------------------------------------------
CommChannel::CommChannel(void)
{
    mFile = NULL;
}

//-----------------------------------------------------------------------------
CommChannel::~CommChannel()
{
    Close();
}

//-----------------------------------------------------------------------------
void CommChannel::Open(int port, int baudrate, int bits, int parity, int stop)
{

    // Communications
    //SetComm  ...

    char fname[10];
    sprintf_s(fname, sizeof(fname), "COM%d", port);
    mFile = CreateFile(fname, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    if (mFile == INVALID_HANDLE_VALUE) 
    {
        unsigned long err = GetLastError();
        mFile = NULL;
        return;
    } 

    COMMCONFIG cc;
    cc.dwSize = sizeof(cc);

    // DCB
    cc.dcb.DCBlength           = sizeof(cc.dcb);
    cc.dcb.BaudRate            = baudrate;             // current baud rate 
    cc.dcb.fBinary             = TRUE;                 // binary mode, no EOF check 
    cc.dcb.fParity             = FALSE;                // enable parity checking 
    cc.dcb.fOutxCtsFlow        = FALSE;                // CTS output flow control 
    cc.dcb.fOutxDsrFlow        = FALSE;                // DSR output flow control 
    cc.dcb.fDtrControl         = DTR_CONTROL_DISABLE;  // DTR flow control type 
    cc.dcb.fDsrSensitivity     = FALSE;                // DSR sensitivity 
    cc.dcb.fTXContinueOnXoff   = FALSE;                // XOFF continues Tx 
    cc.dcb.fOutX               = FALSE;                // XON/XOFF out flow control 
    cc.dcb.fInX                = FALSE;                // XON/XOFF in flow control 
    cc.dcb.fErrorChar          = FALSE;                // enable error replacement 
    cc.dcb.fNull               = FALSE;                // enable null stripping 
    cc.dcb.fRtsControl         = FALSE;                // RTS flow control 
    cc.dcb.fAbortOnError       = FALSE;                // abort reads/writes on error 
    cc.dcb.XonLim              = 1024;                 // transmit XON threshold 
    cc.dcb.XoffLim             = 1024;                 // transmit XOFF threshold 
    cc.dcb.ByteSize            = bits;                 // number of bits/byte, 4-8 
    cc.dcb.Parity              = parity;               // 0-4=no,odd,even,mark,space 
    cc.dcb.StopBits            = ONESTOPBIT;           // 0,1,2 = 1, 1.5, 2 
    cc.dcb.XonChar             = 0;                    // Tx and Rx XON character 
    cc.dcb.XoffChar            = 0;                    // Tx and Rx XOFF character 
    cc.dcb.ErrorChar           = 0;                    // error replacement character 
    cc.dcb.EofChar             = 0;                    // end of input character 
    cc.dcb.EvtChar             = 0;                    // received event character 

    SetCommConfig(mFile, &cc, sizeof(cc));

    COMMTIMEOUTS ct;
    ct.ReadIntervalTimeout         = MAXDWORD; 
    ct.ReadTotalTimeoutMultiplier  = 0; 
    ct.ReadTotalTimeoutConstant    = 0; 
    ct.WriteTotalTimeoutMultiplier = 1; 
    ct.WriteTotalTimeoutConstant   = 100; 

    SetCommTimeouts(mFile, &ct);
    mReceiveTimeout = 0;
}

//-----------------------------------------------------------------------------
void CommChannel::Close(void)
{
    if (mFile == NULL) return;

    CloseHandle(mFile);
    mFile = NULL;
}

//-----------------------------------------------------------------------------
int CommChannel::Send(int numbytes, unsigned char *data)
{
    if (mFile == NULL) return -1;

    unsigned long written = 0;
    int err = WriteFile(mFile, data, numbytes, &written, NULL);

    // if there was an error return nothing written
    if (err == 0) return -1;

    return (int) written;
}

//-----------------------------------------------------------------------------
int CommChannel::Receive(unsigned char *inbyte, int timeout)
{
    if (mReceiveTimeout != timeout)
    {
        mReceiveTimeout = timeout;        

        COMMTIMEOUTS ct;
        if (timeout != 0) 
        {
            ct.ReadIntervalTimeout        = MAXDWORD;
            ct.ReadTotalTimeoutMultiplier = MAXDWORD; 
            ct.ReadTotalTimeoutConstant   = mReceiveTimeout; 
        }
        else              
        {
            ct.ReadIntervalTimeout        = MAXDWORD;
            ct.ReadTotalTimeoutMultiplier = 0; 
            ct.ReadTotalTimeoutConstant   = 0; 
        }

        ct.WriteTotalTimeoutMultiplier = 1; 
        ct.WriteTotalTimeoutConstant   = 100; 

        SetCommTimeouts(mFile, &ct);
    }

    (*inbyte) = 0;    
    if (mFile == NULL) return -1;

    // Communications
    unsigned long read = 0;
    int err = ReadFile(mFile, inbyte, 1, &read, NULL);

    // if there is an error return nothing read
    if (err == 0) return 0;

    return (int) read;
}

//-----------------------------------------------------------------------------
void CommChannel::SetDTR(BOOL enable)
{
    if (enable) EscapeCommFunction(mFile, SETDTR);
    else        EscapeCommFunction(mFile, CLRDTR);
}

