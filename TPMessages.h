//--------------------------------------------------------------------------------------
// File:    TPMessages.h
// Desc:    Defines all messages, notifications, and parameters that can be sent to 
//          plug-ins and components.
//
//          This header will likely grow over time. Additions to this header in future
//          TunerPro versions will not break released plug-ins.
//
// Copyright (c) Mark Mansur. All rights reserved. This source code is *not*
// redistributable, and may be obtained only by permission from its author, Mark Mansur.
// For information, please see www.tunerpro.net, or contact mark@tunerpro.net.
//--------------------------------------------------------------------------------------
#pragma once
#ifndef _TPMESSAGES_H
#define _TPMESSAGES_H

//---------------------------------------------------------------------------------------
// Message: TPM_REGISTER_TUNERPRO_INTERFACE
// Desc:    Sent to plug-in object by TunerPro to register the ITunerProApp interface.
// Param1:  ITunerPro* pInterface. The application interface to be registered.
// Param2:  Not used
//---------------------------------------------------------------------------------------
#define TPM_REGISTER_TUNERPRO_INTERFACE     5000L


//
//---------------------------------------------------------------------------------------
// Messages 0-10000 are reserved
//---------------------------------------------------------------------------------------
//

#endif // _TPMESSAGES_H