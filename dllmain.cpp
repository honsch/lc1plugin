//--------------------------------------------------------------------------------------
// File:    dllmain.cpp
// Desc:    This file implements DLLMain, which is the entry point called by the system
//          when a DLL is loaded into an application's memory space.
//
//          Documentation should have been distributed with the TunerPro Developer SDK.
//          Also make sure to study ITPPlugin.h for more information on the plug-in
//          architecture.
//
// Copyright (c) Mark Mansur. All rights reserved. This source code is *not*
// redistributable, and may be obtained only by permission from its author, Mark Mansur.
// For information, please see www.tunerpro.net, or contact mark@tunerpro.net.
//--------------------------------------------------------------------------------------
#include "stdafx.h"
#include "LC1Plugin.h"

GLOBALS globals;

BOOL APIENTRY DllMain(HMODULE hModule,
                      DWORD   reason,
                      LPVOID  lpReserved)
{
    switch (reason)
    {
        // DLL_PROCESS_ATTACH will be used when TunerPro loads the DLL.
        case DLL_PROCESS_ATTACH:
            // Store off a handle to this instance for other components of this DLL
            globals.hModule = hModule;
            // TODO: Load settings, etc
            break;

        // DLL_PROCESS_DETACH will be used when TunerPro unloads the DLL
        case DLL_PROCESS_DETACH:
            // TODO: Save settings (if necessary - could also be done before returning from 
            // "Configure" calls.
            break;
    }
    return TRUE;
}

