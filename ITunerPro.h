//--------------------------------------------------------------------------------------
// File:    ITunerPro.h
// Desc:    Defines all TunerPro RT application interfaces that are available to
//          plug-ins and their components.
//
// Copyright (c) Mark Mansur. All rights reserved. This source code is *not*
// redistributable, and may be obtained only by permission from its author, Mark Mansur.
// For information, please see www.tunerpro.net, or contact mark@tunerpro.net.
//--------------------------------------------------------------------------------------
#pragma once
#ifndef _ITUNERPRO_H
#define _ITUNERPRO_H

// Header defining all messages and notifications that can be sent by TunerPro to
// plug-in and component objects, and from those object to TunerPro
#include "TPMessages.h"


// Forward declarations
class ITPAppComponent;

//---------------------------------------------------------------------------------------
// These are the types of components that the plug-in can request interfaces for. Only
// one of each type exists, so requesting an interface for a single type multiple times
// will return the same component instance.
//---------------------------------------------------------------------------------------
enum TPAPPCOMPONENT_TYPE
{
    TPAPPCOMPONENT_NONE,                    // No components are currently exposed
    TPAPPCOMPONENT_PROGRESS     = 10000,
};

//---------------------------------------------------------------------------------------
// Structure containing basic information about TunerPro
//---------------------------------------------------------------------------------------
#define TPAI_MAX_VERSION    20

struct TP_APP_INFO
{
    DWORD cbSize;                           // Should be intialized by caller to sizeof(TP_APP_INFO)
    CHAR  strVersion[TPAI_MAX_VERSION];     // Version string
};

//---------------------------------------------------------------------------------------
// ITunerPro - the base application interface. This gets registered by TunerPro with
// your plug-in during the plug-in initialization process.
//---------------------------------------------------------------------------------------
class ITunerProApp
{
public:

    virtual HRESULT GetAppInfo(TP_APP_INFO* pInfoStruct)                                 = 0;

    // Get and release TunerPro components
    virtual ITPAppComponent* GetComponent(IN TPAPPCOMPONENT_TYPE type)                   = 0;
    virtual VOID    ReleaseComponent(IN ITPAppComponent* pComponent)                     = 0;

    virtual LONG_PTR MessageHandler(IN UINT uiMsg, IN LPARAM lParam1, IN LPARAM lParam2) = 0;
};


//---------------------------------------------------------------------------------------
// ITPComponent - the base component interface that gets inherited by all TunerPro
// application interfaces.
//---------------------------------------------------------------------------------------
class ITPAppComponent
{
public:
    virtual TPAPPCOMPONENT_TYPE GetType()               = 0;
};


//---------------------------------------------------------------------------------------
// The main progress bar in the application
//---------------------------------------------------------------------------------------
class ITPProgress : public ITPAppComponent
{
public:

    virtual HRESULT Lock()                                  = 0;
    virtual BOOL    IsLocked()                              = 0;
    virtual HRESULT Unlock()                                = 0;

    virtual BOOL SetRange(INT iMin, INT iMax)               = 0;
    virtual BOOL GetRange(INT& iMin, INT& iMax)             = 0;
    virtual INT  SetCurrentPosition(INT iVal)               = 0;
    virtual INT  GetCurrentPosition()                       = 0;
    virtual INT  Step()                                     = 0;
    virtual HRESULT SetText(CHAR* strText, UINT bcText)     = 0;
    virtual HRESULT GetText(CHAR* strBuffer, UINT bcBuffer) = 0;
};

#endif // _ITUNERPRO_H