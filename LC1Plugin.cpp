//--------------------------------------------------------------------------------------
// File:    LC1Plugin.cpp
// Desc:    Basic single LC1 wideband serial driver
//
// Copyright (c) Eric Honsch. All rights reserved.  Free for any one to use as they see fit.
//--------------------------------------------------------------------------------------
#include "stdafx.h"
#include "LC1Plugin.h"

#include "resource.h"

#include <math.h>

#include <shlobj.h>

// Our global instantiation of the plug-in object.
LC1Plugin    gPlugin;

//---------------------------------------------------------------------------------------
extern "C"
{
    __declspec(dllexport) ITPPlugin *TPCreatePlugin(void)
    {
        return &gPlugin;
    }

    __declspec(dllexport) void TPReleasePlugin(ITPPlugin *plugin)
    {
    }
}

INT_PTR CALLBACK ChooseComProc(_In_  HWND hwndDlg, _In_  UINT uMsg, _In_  WPARAM wParam, _In_  LPARAM lParam);


//---------------------------------------------------------------------------------------
LC1Plugin::LC1Plugin(void)
{
}

//---------------------------------------------------------------------------------------
LC1Plugin::~LC1Plugin()
{
}

//---------------------------------------------------------------------------------------
BOOL LC1Plugin::GetPluginInfo(TPP_PLUGININFO *info)
{
    //OutputDebugString("LC1Plugin::GetPluginInfo called");
    if (info == NULL) return FALSE;

    info->dwContractVersion = TPPLUGIN_CONTRACT_VERSION; 
    UuidFromString((RPC_CSTR)PLUGIN_GUID, &info->ID);
    StringCbCopy(info->strName, TPP_MAX_NAME, TEXT("LC1 Reader"));
    StringCbCopy(info->strDesc, TPP_MAX_DESC, TEXT("This plug-in will read from a single Innovate Motorsports LC1 ONLY"));
    StringCbCopy(info->strVersion, TPP_MAX_VERSION, TEXT("1.0.0"));
    StringCbCopy(info->strAuthor, TPP_MAX_AUTHOR, TEXT("Eric Honsch"));
    info->dwComponentCount   = 1;
    info->bConfigurable      = FALSE;
    info->wVersionMajor      = 1;
    info->wVersionMinor      = 0;
    return TRUE;
}

//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
HRESULT LC1Plugin::Configure(TPP_CONFIGINFO *config)
{
    //OutputDebugString("LC1Plugin::Configure called");
    return S_OK;
}

//--------------------------------------------------------------------------------------
ITPPluginComponent *LC1Plugin::GetComponent(UINT index)
{
    //OutputDebugString("LC1Plugin::GetComponent called");
    if (index == LC1_DRIVER) return &mLC1Driver;

    return NULL;
}

//---------------------------------------------------------------------------------------
VOID LC1Plugin::ReleaseComponent(ITPPluginComponent *component)
{
    //OutputDebugString("LC1Plugin::ReleaseComponent called");
}

//---------------------------------------------------------------------------------------
LONG_PTR LC1Plugin::MessageHandler(UINT msg, LPARAM param1, LPARAM param2)
{
    //OutputDebugString("LC1Plugin::MessageHandler called");
    switch (msg)
    {
        case TPM_REGISTER_TUNERPRO_INTERFACE:
            mITunerPro = (ITunerProApp *) param1;
            break;
    }
    return 0;
}


//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------
LC1Driver::LC1Driver(void)
{
    mTimeout.cbSize            = sizeof(mTimeout);
    mTimeout.dwFlags           = 0;
    mTimeout.dwReadInterval    = 0;
    mTimeout.dwReadTotalConst  = 0;
    mTimeout.dwReadTotalMult   = 0;
    mTimeout.dwWriteTotalConst = 0;
    mTimeout.dwWriteTotalMult  = 0;

    mFrameWritePtr = 0;
    
    mComPort = 1;

    mErrorPrompt    = FALSE;
    mLastError[0]   = '\0';

    // Build the config file name
    SHGetFolderPath(NULL, CSIDL_PROFILE, NULL, SHGFP_TYPE_CURRENT, mConfigFileName);
    strcat_s(mConfigFileName, sizeof(mConfigFileName), "\\My Documents\\TunerPro Files\\Plugins\\LC1Plugin.cfg");
    LoadConfig();
}

//---------------------------------------------------------------------------------------
LC1Driver::~LC1Driver()
{
}

//---------------------------------------------------------------------------------------
TPPCOMPONENT_TYPE LC1Driver::GetType(void)
{ 
    return TPPLUGINCOMPONENT_DATAACQIO_DRIVER; 
}

//---------------------------------------------------------------------------------------
BOOL LC1Driver::GetComponentInfo(TPP_COMPONENTINFO *info)
{
    //OutputDebugString("LC1 Driver::GetComponentInfo called");
    if (info == NULL)  return FALSE;

    info->cbSize = sizeof(*info);

    UuidFromString((RPC_CSTR)LC1_GUID, &info->ID);
    StringCbCopy(info->strName, TPP_MAX_NAME, TEXT("LC1 IO Driver"));
    StringCbCopy(info->strDesc, TPP_MAX_DESC, TEXT("A simple, limited LC1 driver that supports a single LC1"));
    StringCbCopy(info->strVersion, TPP_MAX_VERSION, TEXT("1.0.0"));

    info->bConfigurable = TRUE;

    // Provide TunerPro with an easy-to-compare set of version numbers. Strings aren't easy
    // to compare, but are easy for users to read.
    info->wVersionMajor = 1;
    info->wVersionMinor = 0;
    return TRUE;
}

//---------------------------------------------------------------------------------------
HRESULT LC1Driver::Configure(TPP_CONFIGINFO *config)
{
    //OutputDebugString("LC1Driver::Configure called");

    if (config == NULL)                    return S_OK;
    if (config->cbSize != sizeof(*config)) return E_INVALIDARG;

    CreateDialogParam((HINSTANCE) globals.hModule, 
                      MAKEINTRESOURCE(IDD_CHOOSE_COM), 
                      config->hwndOwner, 
                      (DLGPROC) &ChooseComProc,
                      (LPARAM) this);

    return S_OK;
}

//---------------------------------------------------------------------------------------
LONG_PTR LC1Driver::MessageHandler(UINT msg, LPARAM param1, LPARAM param2)
{
    return 0;
}

//---------------------------------------------------------------------------------------
const CHAR *LC1Driver::GetLastErrorText(BOOL &prompt)
{
    //OutputDebugString("LC1Driver::GetLastErrorText called");

    prompt = mErrorPrompt;
    return mLastError;
}


//---------------------------------------------------------------------------------------
HRESULT LC1Driver::GetHardwareInfo(TPDATAACQIOCAPS *caps)
{
    //OutputDebugString("LC1Driver::GetHardwareInfo called");

    if (caps == NULL)                  return E_INVALIDARG;
    if (caps->cbSize != sizeof(*caps)) return E_INVALIDARG;

    StringCbCopy(caps->strName, TPP_MAX_NAME, "LC1 reader");
    caps->strDescription[0] = 0;
    caps->strVersion[0] = 0;
    return S_OK;
}

//---------------------------------------------------------------------------------------
HRESULT LC1Driver::InitializeHardware(TPDATAACQHWINIT *init)
{
    //OutputDebugString("LC1Driver::GetHardwareInfo called");

    if (init == NULL)                  return E_INVALIDARG;
    if (init->cbSize != sizeof(*init)) return E_INVALIDARG;

    mConnected = true;

    memset(mInnovateFrame, 0, sizeof(mInnovateFrame));

    mFrameWritePtr = 0;
    mCommChannel.Open(mComPort, 19200, 8, 0, 1);

    return S_OK;
}

//---------------------------------------------------------------------------------------
HRESULT LC1Driver::ReleaseHardware(void)
{
    //OutputDebugString("LC1Driver::ReleaseHardware called");

    mCommChannel.Close();
    mFrameWritePtr = 0;
    mConnected = false;

    return S_OK;
}

//---------------------------------------------------------------------------------------
HRESULT LC1Driver::WriteData(TPDATAACQWRITE *writeInfo)
{
    //OutputDebugString("LC1Driver::WriteData called");

    if (writeInfo == NULL) return E_INVALIDARG;
    if (writeInfo->cbSize != sizeof(*writeInfo)) return E_INVALIDARG;

    HRESULT hr = S_OK;
    DWORD dwIO = 0;

    // Optionally, tell TunerPro how much data was written.
    if (writeInfo->puiTransferred)
    {
        *writeInfo->puiTransferred = dwIO;
    }

    return hr;
}


#define INNOVATE_HEADER_MARKER_BE   0x80A0

#define INNOVATE_LC1_MARKER_MASK    0x80C080E2 
#define INNOVATE_LC1_MARKER_TEST    0x00000042

#define INNOVATE_LC1_STATUS_VALID               0
#define INNOVATE_LC1_STATUS_O2_PERCENT          1
#define INNOVATE_LC1_STATUS_CALIBRATING         2
#define INNOVATE_LC1_STATUS_NEED_CALIBRATION    3
#define INNOVATE_LC1_STATUS_WARMING_UP          4
#define INNOVATE_LC1_STATUS_HEATER_CALIBRATING  5
#define INNOVATE_LC1_STATUS_ERROR               6
#define INNOVATE_LC1_STATUS_RESERVED            7

// FU MICROSOFT!  These are in the C Standard!
typedef unsigned short uint16_t;
typedef unsigned       uint32_t;

//---------------------------------------------------------------------------------------
HRESULT LC1Driver::ReadData(TPDATAACQREAD *readInfo)
{
    //OutputDebugString("LC1Driver::ReadData called");

    if (readInfo == NULL)                      return E_INVALIDARG;
    if (readInfo->cbSize != sizeof(*readInfo)) return E_INVALIDARG;
    if (readInfo->pBuffer == NULL)             return E_INVALIDARG;
    if (readInfo->cbReadSize < sizeof(float))  return E_INVALIDARG;

    // pre-set the amount read to zero for error cases
    if (readInfo->puiTransferred != NULL) *(readInfo->puiTransferred) = 0;

#if 0
    // Hacky test value oscillating arounf 14.7
    static float faketime = 0.0F;
    if (readInfo->puiTransferred != NULL) *(readInfo->puiTransferred) = sizeof(unsigned);
    float faked = 14.7F + (3.0F * sinf(faketime));
    faked *= 65536.0F;
    unsigned fakedfixed = (unsigned) faked;
    faketime += 0.03F;
    memcpy_s(readInfo->pBuffer, readInfo->cbReadSize, &fakedfixed, sizeof(unsigned));
    return S_OK;
#endif


    // Fill the fifo with all data available from serial port
    unsigned char val;
    while (mFrameWritePtr < INNOVATE_MAX_FRAME_SIZE)
    {
        int count = mCommChannel.Receive(&val, 0);
        if (count != 1) break;
        mInnovateFrame[mFrameWritePtr++] = val;
    }
     
    // Do we have enough data?
    // Four bytes minimum in a valid frame
    if (mFrameWritePtr < 4)
    {
        return HRESULT_FROM_WIN32(ERROR_NOT_READY);
    }

    // Find the header and move it to the front of the window
    unsigned headerStart = 0;
    BOOL haveHeader = FALSE;
    while (headerStart < (mFrameWritePtr - 1))
    { 
        uint16_t header = *((uint16_t *) (mInnovateFrame + headerStart));
        uint16_t header_masked = header & INNOVATE_HEADER_MARKER_BE;
        if (header_masked == INNOVATE_HEADER_MARKER_BE)
        {

            haveHeader = TRUE;
            break;
        }
        ++headerStart;            
    }

    // If we haven't found a header, flush it all except the last byte which MIGHT be the first byte of a header
    // then we return an appropriate error
    if (haveHeader == FALSE)
    {
        mInnovateFrame[0] = mInnovateFrame[mFrameWritePtr - 1];
        mFrameWritePtr = 1;
        return HRESULT_FROM_WIN32(ERROR_NOT_READY);
    }

    //OutputDebugString("LC1Driver::ReadData - Found a good Header");

    
    // Move the header to the front of the window
    if ((haveHeader = TRUE) && (headerStart != 0))
    {
        memmove_s(mInnovateFrame, sizeof(mInnovateFrame), mInnovateFrame + headerStart, mFrameWritePtr - headerStart);
        headerStart = 0;
    }

    // frame size is in words in the header, convert to bytes
    // Don't forget the header size!
    unsigned frameSize = 2 + ((mInnovateFrame[1] & 0x7F) + (((mInnovateFrame[0]) & 0x01) << 8) * 2);

    // If we don't have the whole frame, we can return now withno data read
    if (mFrameWritePtr < (frameSize + 2))
    {
        return HRESULT_FROM_WIN32(ERROR_NOT_READY);
    }
    
    //OutputDebugString("LC1Driver::ReadData - Got a complete frame");

    // Search through the frame for an LC1
    BOOL haveLC1 = FALSE;
    unsigned lc1Start = 2;
    uint32_t lc1;
    while (lc1Start <= (frameSize + 2 - 4))  // the +2 is for the header
    {
        lc1 = *((uint32_t *) (mInnovateFrame + lc1Start));
        uint32_t lc1_masked = lc1 & INNOVATE_LC1_MARKER_MASK;
        if (lc1_masked == INNOVATE_LC1_MARKER_TEST)
        {
            haveLC1 = TRUE;
            break;
        }
        ++lc1Start;
    }


    // move the data down to step over this frame and update the write pointer
    memmove_s(mInnovateFrame, sizeof(mInnovateFrame), mInnovateFrame + frameSize + 2, mFrameWritePtr); // - (frameSize + 2));
    mFrameWritePtr -= (frameSize + 2);

    // If there's no LC1 in the frame return the appropriate error        
    if (haveLC1 == FALSE)
    {
        return HRESULT_FROM_WIN32(ERROR_NOT_CONNECTED);
    }

    //OutputDebugString("LC1Driver::ReadData - Found an LC-1");

    unsigned status = (lc1 >> 2) & 0x7;
    unsigned lambda = ((lc1 >> 24) & 0x7F) | ((lc1 >> 9) & 0x1F80);
    unsigned afMult = ((lc1 >>  8) & 0x7F) | ((lc1 << 7) & 0x80);

    switch (status)
    {
        case INNOVATE_LC1_STATUS_VALID:
        {
            //OutputDebugString("LC1Driver::ReadData - LC-1 Data is good");
            if (readInfo->puiTransferred != NULL) *(readInfo->puiTransferred) = 4;
            float l = 0.5F + (0.001F * (float) lambda);
            float afm = 0.1F * (float) afMult;
            float afr = l * afm;
            unsigned afrfixed = (unsigned) (afr * 65536.0F);

            memcpy_s(readInfo->pBuffer, readInfo->cbReadSize, &afrfixed, sizeof(unsigned));
            return S_OK;
        }

        // I should put a descriptive error in the plugin's error string here
        case INNOVATE_LC1_STATUS_O2_PERCENT:
        case INNOVATE_LC1_STATUS_CALIBRATING:
        case INNOVATE_LC1_STATUS_WARMING_UP:
        case INNOVATE_LC1_STATUS_HEATER_CALIBRATING:
            //OutputDebugString("LC1Driver::ReadData - LC-1 isn't ready");
            return HRESULT_FROM_WIN32(ERROR_NOT_READY);

        // I should put a descriptive error in the plugin's error string here as well
        case INNOVATE_LC1_STATUS_NEED_CALIBRATION:
        case INNOVATE_LC1_STATUS_ERROR:
        case INNOVATE_LC1_STATUS_RESERVED:
        default:
            //OutputDebugString("LC1Driver::ReadData - LC-1 is borked");
            return E_FAIL;
    }

    return E_FAIL;
}

//---------------------------------------------------------------------------------------
HRESULT LC1Driver::SetTimeouts(TPDATAACQTIMEOUTS *timeout)
{
    //OutputDebugString("LC1Driver::SetTimeouts called");

    if (timeout == NULL)                     return E_INVALIDARG;
    if (timeout->cbSize != sizeof(*timeout)) return E_INVALIDARG;

    mTimeout = *timeout;
    return S_OK;
}

//---------------------------------------------------------------------------------------
HRESULT LC1Driver::GetTimeouts(TPDATAACQTIMEOUTS *timeout)
{
    if (timeout == NULL)                     return E_INVALIDARG;
    if (timeout->cbSize != sizeof(*timeout)) return E_INVALIDARG;

    *timeout = mTimeout;
    return S_OK;
}

//---------------------------------------------------------------------------------------
HRESULT LC1Driver::PurgeReceiveBuffer(void)
{
    OutputDebugString("LC1Driver::PurgeReceiveBuffer called");

    return S_OK;
}

//---------------------------------------------------------------------------------------
HRESULT LC1Driver::PurgeTransmitBuffer(void)
{
    OutputDebugString("LC1Driver::PurgeTransmitBuffer called");

    return S_OK;
}

//---------------------------------------------------------------------------------------
void LC1Driver::SetComPort(unsigned port)
{
    if (port == mComPort) return;

    bool connected = mConnected;
    if (connected) ReleaseHardware();

    mComPort = port;
    SaveConfig();

    if (connected)
    {
        TPDATAACQHWINIT init;
        init.cbSize = sizeof(init);
        InitializeHardware(&init);
    }
}

//---------------------------------------------------------------------------------------
void LC1Driver::LoadConfig(void)
{
    FILE *inf;
    fopen_s(&inf, mConfigFileName, "rt");
    if (inf == NULL)
    {
        strcpy_s(mLastError, sizeof(mLastError), "Cannot open config file for reading");
        return;
    }

#define SEPARATORS " =\t"

    while (!feof(inf))
    {
        char line[512];
        char *context;
        line[0] = 0;
        fgets(line, sizeof(line), inf);
        char *token = strtok_s(line, SEPARATORS, &context);
        if (token == NULL) continue;
 
        if (0 == _stricmp(token, "COMPORT"))
        {
            token = strtok_s(NULL, SEPARATORS, &context);
            if (token == NULL) continue;
            unsigned port = atoi(token);
            if ((port > 0) && (port < 256)) mComPort = port;
        }
    }
    fclose(inf);
}

//---------------------------------------------------------------------------------------
void LC1Driver::SaveConfig(void)
{
    FILE *outf;
    fopen_s(&outf, mConfigFileName, "wt");
    if (outf == NULL)
    {
        strcpy_s(mLastError, sizeof(mLastError), "Cannot open config file for writing");
        return;
    }

    fprintf(outf, "COMPORT %d\n", mComPort);
    fclose(outf);
}

//---------------------------------------------------------------------------------------
// Functions for Config Dialog box
//
INT_PTR CALLBACK ChooseComProc(_In_  HWND dialog,
                               _In_  UINT msg,
                               _In_  WPARAM wparam,
                               _In_  LPARAM lparam)
{
    switch (msg)
    {

        case WM_INITDIALOG:
        {
            SetWindowLongPtr(dialog, DWLP_USER, lparam);
            LC1Driver *owner = (LC1Driver *) lparam;
            unsigned a;
            for (a = 1; a < 13; a++)
            {
                char s[10];
                sprintf_s(s, sizeof(s) , "Com%d", a);
                SendDlgItemMessage(dialog, IDC_COMLIST, LB_ADDSTRING, 0, (LPARAM) s);
            }
            
            unsigned port = owner->GetComPort();
            SendDlgItemMessage(dialog, IDC_COMLIST, LB_SETCURSEL, (LPARAM) port - 1, 0);

            ShowWindow(dialog, SW_SHOW);
            return TRUE;
        }

        case WM_COMMAND:
        {
            unsigned control = LOWORD(wparam);

            if (control == IDCANCEL)
            {
                DestroyWindow(dialog);
                return true;
            }

            if (control == IDOK)
            {
                unsigned port = SendDlgItemMessage(dialog, IDC_COMLIST, LB_GETCURSEL, 0, 0);
                LC1Driver *owner = (LC1Driver *) GetWindowLongPtr(dialog, DWLP_USER);
                if ((owner != NULL) && (port != LB_ERR))
                {
                    owner->SetComPort(port + 1);
                }
                DestroyWindow(dialog);
            }
            break;
        }

        default:
            return FALSE;

    }

    return FALSE;
}
