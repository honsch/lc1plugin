//-----------------------------------------------------------------------------
//  Comm.h
//  Brain-dead serial port wrapper for Win32
//  Copyright Eric Honsch.  Free for any one to use as they see fit
//-----------------------------------------------------------------------------


#ifndef COMM_H
#define COMM_H

#ifndef _WINDOWS_
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif


//-----------------------------------------------------------------------------
class CommChannel 
{
  public:

    CommChannel(void);
   ~CommChannel();

    void Open(int port, int baudrate, int bits, int parity, int stop);
    void Close(void);

    int  Send(int numbytes, unsigned char *data);
    int  Receive(unsigned char *inbyte, int timeout = 0);

    void SetDTR(BOOL enable);

  private:

    HANDLE mFile;
    int    mReceiveTimeout;
};


#endif // COM_H